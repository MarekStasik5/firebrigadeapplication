package mainPackage;


import FirefighterData.FireFirefighter;
import GeneralEquipment.Car;

import GeneralEquipment.FireTrack;
import GeneralEquipment.Garage;
import GeneralEquipment.TechnicalTrack;
import initailData.AdditionalFunctions;
import java.util.HashMap;
import java.util.Map;



public class Main {

    public static void main(String[] args){
    	
    
    	AdditionalFunctions.generatedExampleFireFirefighter(8);
    	AdditionalFunctions.generatedExampleParamedic(7);
    	
    	FireFirefighter.loadFirefighters(); 	      
        FireFirefighter.deleteEveryXfirefighter(5);
        FireFirefighter.ageAverage();
        FireFirefighter.sortByAge();     
       // FireFirefighter.showList();        
        
        

        Car<FireTrack> fireTrack = new Car<>(new FireTrack());
        Car<TechnicalTrack> technicalTrack = new Car<>(new TechnicalTrack());
        
        fireTrack.setName("Star");
        fireTrack.setProductionDate(1998);
        fireTrack.setGarage(Garage.BIG_GARAGE);
        
        technicalTrack.setName("Volvo");
        technicalTrack.setProductionDate(2005);
        technicalTrack.setGarage(Garage.SMALL_GARAGE);
       
        
        
        Map<String,String> assignTrackToGarage=new HashMap<>();
        assignTrackToGarage.put(fireTrack.getName(), fireTrack.getGarage().getDescription());
        assignTrackToGarage.put(technicalTrack.getName(), technicalTrack.getGarage().getDescription());                
            
    }

}
