package GeneralEquipment;

import java.util.ArrayList;
import java.util.List;

public class CarEquipment {

    public String name;
    public int productionDate;
    public int number;


    public CarEquipment(String name, int productionDate, int number) {
        this.name = name;
        this.productionDate = productionDate;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public int getProductionDate() {
        return productionDate;
    }

    public int getNumber() {
        return number;
    }

    List<CarEquipment> carEquipment= new ArrayList<>();

        public void addToList(CarEquipment device){
        carEquipment.add(device);
        }

    public void printList(){
        for (CarEquipment device:carEquipment) {
            System.out.println(device.getName());

        }
    }

}
