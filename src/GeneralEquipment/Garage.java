package GeneralEquipment;

public enum Garage {
	BIG_GARAGE("BIG_GARAGE"),
	SMALL_GARAGE("SMALL_GARAGE");
	
	private String description;

	private Garage(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	
}
