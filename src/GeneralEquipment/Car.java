package GeneralEquipment;


public class Car<T> {
    public T car;
    Garage garage;
    public String name;
    public int productionDate;

    public Car(T car){
        this.car=car;
    }

    public T getCar(){
        return car;
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getProductionDate() {
		return productionDate;
	}

	public void setProductionDate(int productionDate) {
		this.productionDate = productionDate;
	}

	public Garage getGarage() {
		return garage;
	}

	public void setGarage(Garage garage) {
		this.garage = garage;
	}
    
    
   

}
