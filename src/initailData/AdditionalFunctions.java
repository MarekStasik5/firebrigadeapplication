package initailData;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

import FirefighterData.FireFirefighter;

public class AdditionalFunctions {
	
	FireFirefighter firefighter;
	
	public static Scanner x;
    public final static  String filePathFireFirefighters = "./src/fireFirefighters.txt";
    public final static String filePathParamedic = "./src/paramedics.txt";
    public static String name;
    public static String lastName;
    public static int age;
	private static	int maxAge=60;
	private static	int minAge=18;
	
	public static void generatedExampleFireFirefighter(int quantity) {
    	openFileToWriteFireFirefighter(quantity);
    }
    
    public static void generatedExampleParamedic(int quantity) {
    	openFileToWriteParamedicFirefighter(quantity);
    }
	
    public static void openFileToWriteFireFirefighter(int quantity){   	
    	try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filePathFireFirefighters))) {
    		Random age = new Random();
    		int i=1;
        	while(i<=quantity) {
        		bufferedWriter.write("name" + i + " "+ "lastname"+i+" "+(age.nextInt(maxAge-minAge)+minAge));
        		bufferedWriter.newLine();
        		i++;
        	}
        } catch (IOException e) {
            System.out.println("File not opened");
            e.printStackTrace();
        }
    }

    public static void openFileToWriteParamedicFirefighter(int quantity){   	
    	try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(filePathParamedic))) {
    		Random age = new Random();
    		int i=1;
        	while(i<=quantity) {
        		int value=age.nextInt(maxAge-minAge)+minAge;
        		bufferedWriter.write("name"+i +" " + "lastname"+i +" "+ value + " " + paramedicExperience()+" "+(value-minAge ));
        		bufferedWriter.newLine();
        		i++;
        	}
        } catch (IOException e) {
            System.out.println("File not opened");
            e.printStackTrace();
        }
    }
    
    private static String paramedicExperience() {
    	Random r= new Random();
    	int value=r.nextInt(3);
    	String typeOfEducation=new String();
    	switch(value) {
    		case 0:
    			typeOfEducation="MEDICAL_STUDY";
    			break;
    		case 1:
    			typeOfEducation="ADDITIONAL_COURSE";
    			break;
    		case 2:
    			typeOfEducation="NURSE_STUDY";
    			break;
    		case 3:
    			typeOfEducation="PARAMEDIC_STUDY";
    			break;
    		default:
    			throw new IllegalArgumentException("Something was wrong ");
    	}
		return typeOfEducation;
    }

}
