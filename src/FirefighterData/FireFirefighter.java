package FirefighterData;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import initailData.AdditionalFunctions;

public class FireFirefighter extends Firefighter{

	public static Scanner x;
   	public static List<FireFirefighter> firefighterList=new LinkedList<>();
	
   	public FireFirefighter() {}
   	
    public FireFirefighter(String name, String lastName, int age){
        super(name, lastName, age);
    }
   	
   	public static void loadFirefighters() {
    	openFileToRaedFireFirefighter();
    	readFile();
    	closeFile();
    }
    
    public static void openFileToRaedFireFirefighter(){
        try {
            x =new Scanner(new File(AdditionalFunctions.filePathFireFirefighters));
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        }
    }
    
    public static List<FireFirefighter> readFile(){
        while (x.hasNext()){
        	FireFirefighter firefighter=new FireFirefighter();
            firefighter.setName(x.next());
            firefighter.setLastName(x.next());
            firefighter.setAge(x.nextInt());                
            firefighterList.add(firefighter);
        }
        return firefighterList;
    }
    
    public static void closeFile(){
        x.close();
    }
    
    public static void showList() {
    	for(FireFirefighter firefighterTMP: firefighterList) {
    		System.out.println(firefighterTMP.getLastName());
    	}
    }
   
   public static List<FireFirefighter> deleteEveryXfirefighter(int x) {   
	   for(int i=0;i+x-1<=firefighterList.size()-1;i++) {
		   firefighterList.remove(x-1);
	   }
	   return firefighterList;
   }
   
   public static double ageAverage() {
	   double value=0;
	   int sumAge=0, i=0;
	   for(FireFirefighter firefighterTMP:firefighterList) {
		   sumAge+=firefighterTMP.getAge();
		   i++;
	   } 
	   value=(double)sumAge/(double)i;
	   System.out.format("Age average: %.2f%n",value);
	   return value;
   }
   
   public static List<FireFirefighter> sortByAge(){
	   firefighterList.sort(Comparator.comparing(FireFirefighter::getAge));
	   return firefighterList;
   }

}
