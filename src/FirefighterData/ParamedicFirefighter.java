package FirefighterData;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import initailData.AdditionalFunctions;

public class ParamedicFirefighter extends Firefighter implements ParamedicDetails {

	public static Scanner x;
	public static List<ParamedicFirefighter> paramedicList=new ArrayList<>();
    ParamedicEducation education;
    private int paramedicExperience;
    
    public ParamedicFirefighter() {}

    public ParamedicFirefighter(String name, String lastName, int age, ParamedicEducation education ){
        super(name, lastName, age);
        this.education=education;
    }

    @Override
    public int setParamedicExperience(int years) {
    	this.paramedicExperience=years;
        return paramedicExperience;
    }
    
    public static void openFileToRaedParamedic(){
        try {
            x =new Scanner(new File(AdditionalFunctions.filePathFireFirefighters));
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        }
    }
    
    public static List<ParamedicFirefighter> readFile(){
        while (x.hasNext()){
        	ParamedicFirefighter paramedic=new ParamedicFirefighter();
            paramedic.setName(x.next());
            paramedic.setLastName(x.next());
            paramedic.setAge(x.nextInt());
            paramedic.setParamedicExperience(x.nextInt());
            paramedicList.add(paramedic);
        }
        return paramedicList;
    }
    
    public static void closeFile(){
        x.close();
    }
    
    public static void showList() {
    	for(ParamedicFirefighter paramedicTMP: paramedicList) {
    		System.out.println(paramedicTMP.getLastName());
    	}
    }    

   public static Scanner getX() {
	   return x;
   }

   public static void setX(Scanner x) {
	   ParamedicFirefighter.x = x;
   }

   public static List<ParamedicFirefighter> getParamedicList() {
	   return paramedicList;
   }

   public static void setParamedicList(List<ParamedicFirefighter> paramedicList) {
	   ParamedicFirefighter.paramedicList = paramedicList;
   }

   public ParamedicEducation getEducation() {
	   return education;
   }

   public void setEducation(ParamedicEducation education) {
	   this.education = education;
   }

   public int getParamedicExperience() {
	   return paramedicExperience;
   }

   
}
