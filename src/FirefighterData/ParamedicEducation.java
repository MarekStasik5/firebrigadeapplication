package FirefighterData;


public enum ParamedicEducation {

    MEDICAL_STUDY("MEDICAL_STUDY"),
    ADDITIONAL_COURSE("ADDITIONAL_COURSE"),
    NURSE_STUDY("NURSE_STUDY"),
    PARAMEDIC_STUDY("PARAMEDIC_STUDY");
    
    private String eduType;
    
    private ParamedicEducation(String eduType) {
    	this.eduType=eduType;
    }

	public String getEduType() {
		return eduType;
	}

	public String setEduType(String eduType) {
		this.eduType = eduType;
		return eduType;
	}

    
    
    
    
}
